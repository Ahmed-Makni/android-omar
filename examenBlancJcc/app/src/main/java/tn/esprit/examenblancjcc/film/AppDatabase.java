package tn.esprit.examenblancjcc.film;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = { film.class }, version = 1, exportSchema = false)
public  abstract class AppDatabase extends RoomDatabase {
    public abstract filmdao filmdao();

}
