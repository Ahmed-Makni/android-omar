package tn.esprit.examenblancjcc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences =this.getSharedPreferences("connected", Context.MODE_PRIVATE);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.addFilmFragment);
        if(fragment == null){
            getSupportFragmentManager().beginTransaction().add(R.id.addFilmFragment, addFilmFragment.newInstance()).commit();
        }
        Fragment fragment2 = getSupportFragmentManager().findFragmentById(R.id.listFilmFragment);
        if(fragment == null){
            getSupportFragmentManager().beginTransaction().add(R.id.listFilmFragment, ListFilmFragment.newInstance()).commit();
        }

    }

    public void onClickLogout(View view) {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        SharedPreferences.Editor editor= preferences.edit();
        editor.clear();
        editor.apply();
        finish();

    }

}
