package tn.esprit.examenblancjcc.film;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface filmdao {

    @Query("Select * FRom film")
    List<film> getallpays();
    @Insert
    void inserall(film films);
    @Delete
    void delete(film film);
    @Update
    void updte(film film);
}
