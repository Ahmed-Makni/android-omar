package tn.esprit.examenblancjcc.film;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import java.io.Serializable;
import java.util.List;

import tn.esprit.examenblancjcc.DescriptionActivity;
import tn.esprit.examenblancjcc.R;

public class filmAdapter extends RecyclerView.Adapter<filmAdapter.ViewHolder> {
    private Context mContext;
    private List<film> paysList;
    @Override
    public filmAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = LayoutInflater.from(mContext).inflate(R.layout.row_film, parent, false);
        return new ViewHolder(mItemView);
    }
    @Override
    public void onBindViewHolder(final filmAdapter.ViewHolder holder, final int position) {
        final film singleItem = paysList.get(position);
        holder.txtPays.setText(singleItem.getName());
        if (singleItem.getOption().equals("premiere"))
        {
            holder.img.setImageResource(R.drawable.ic_grade_red_700_48dp);

        }
        else holder.img.setImageResource(R.drawable.ic_lock_outline_red_900_48dp);

        holder.line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(mContext, DescriptionActivity.class);
                intent.putExtra("your_extra",singleItem.getDescription());
                intent.putExtra("innt",position);
                intent.putExtra("film", (Serializable) singleItem);

                mContext.startActivity(intent);
            }
        });



    }
    @Override
    public int getItemCount() {
        return paysList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtPays;
        private ImageView img ;
        LinearLayout line;

        //A changer -> filmAdapter
        public ViewHolder(View itemView) {
            super(itemView);

            txtPays = itemView.findViewById(R.id.na);
            img = itemView.findViewById(R.id.imageView);
            line = itemView.findViewById(R.id.ll);

        }
    }

    public filmAdapter(Context mContext, List<film> paysList) {
        this.mContext = mContext;
        this.paysList = paysList;
    }

}
