package tn.esprit.examenblancjcc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import tn.esprit.examenblancjcc.film.AppDatabase;
import tn.esprit.examenblancjcc.film.film;

public class DescriptionActivity extends AppCompatActivity {


    private Button sup;
    private TextView dessc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        final Bundle B =getIntent().getExtras();
        final AppDatabase db = Room.databaseBuilder(getApplicationContext(),AppDatabase.class,"phhhh")
                .allowMainThreadQueries()
                .build();
        dessc = findViewById(R.id.de);
        sup = findViewById(R.id.supp);

        dessc.setText(B.getString("your_extra"));

        sup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                film film= (tn.esprit.examenblancjcc.film.film) B.get("film");
                System.out.println("aaaaaaa"+film);

                db.filmdao().delete(film);
                Intent intent = new Intent(DescriptionActivity.this, MainActivity.class);
                startActivity(intent);

            }
        });
    }
}
