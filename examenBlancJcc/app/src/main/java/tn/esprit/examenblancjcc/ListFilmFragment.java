package tn.esprit.examenblancjcc;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import tn.esprit.examenblancjcc.film.AppDatabase;
import tn.esprit.examenblancjcc.film.film;
import tn.esprit.examenblancjcc.film.filmAdapter;

public class ListFilmFragment extends Fragment {

    public ArrayList<film> films=new ArrayList<>();
    private AppDatabase db;
    private View view;

    public ListFilmFragment() {
        // Required empty public constructor
    }


    public static ListFilmFragment newInstance() {
        ListFilmFragment fragment = new ListFilmFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        db= Room.databaseBuilder(getContext(), AppDatabase.class,"phhhh")
                .allowMainThreadQueries()
                .build();

        films= (ArrayList<film>) db.filmdao().getallpays();
         view= inflater.inflate(R.layout.fragment_list_film, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new filmAdapter(getActivity(),films));
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        films= (ArrayList<film>) db.filmdao().getallpays();
        System.out.println(films);
    }
}
