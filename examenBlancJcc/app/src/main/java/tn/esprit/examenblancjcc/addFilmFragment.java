package tn.esprit.examenblancjcc;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import tn.esprit.examenblancjcc.film.AppDatabase;
import tn.esprit.examenblancjcc.film.film;


public class addFilmFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Button btnAddFilm;
    EditText etNom;
    EditText etDescription;
    Spinner spinner;
    AppDatabase db;

    public addFilmFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static addFilmFragment newInstance() {
        addFilmFragment fragment = new addFilmFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_add_film, container, false);

        db= Room.databaseBuilder(view.getContext(),AppDatabase.class,"phhhh")
                .allowMainThreadQueries()
                .build();

        spinner=view.findViewById(R.id.spinn);
        final String[] typesFilm = {"premiere","projection"};
        ArrayAdapter<String> adapter=new ArrayAdapter<>(getContext(),R.layout.item_spinner,R.id.tvSpinner,typesFilm);
        spinner.setAdapter(adapter);
        etNom=view.findViewById(R.id.txtNom);
        etDescription=view.findViewById(R.id.txtDescriptionFilm);
        btnAddFilm=view.findViewById(R.id.btnAjouter);

        return view;
    }

//    // raffrichir un fragment
//    Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragList);
//    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                    fragmentTransaction.detach(currentFragment);
//                    fragmentTransaction.attach(currentFragment);
//                    fragmentTransaction.commit();
    @Override
    public void onStart() {
        super.onStart();
        btnAddFilm.setOnClickListener((event)->{
            System.out.println(etNom.getText().toString());
            System.out.println(etDescription.getText().toString());
            System.out.println(spinner.getSelectedItem().toString());
            film film=new film();
            film.setDescription(etDescription.getText().toString());
            film.setName(etNom.getText().toString());
            film.setOption(spinner.getSelectedItem().toString());
            System.out.println(film);
            db.filmdao().inserall(film);
        });
    }
}
