package tn.esprit.examen2019login;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import tn.esprit.examen2019login.Produit.AppDatabase;
import tn.esprit.examen2019login.Produit.Produit;
import tn.esprit.examen2019login.Produit.ProduitAdapter;


public class ListProduitFragment extends Fragment {


    private AppDatabase db;
    private List<Produit> produits=new ArrayList<>();

    public ListProduitFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ListProduitFragment newInstance() {
        ListProduitFragment fragment = new ListProduitFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_list_produit, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recycle);
        db= Room.databaseBuilder(getContext(), AppDatabase.class,"phhhh")
                .allowMainThreadQueries()
                .build();

        produits= db.produitdao().getallpays();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new ProduitAdapter(getActivity(),produits));
        return view;
    }

}
