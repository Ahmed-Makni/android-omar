package tn.esprit.examen2019login.Produit;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.List;

import tn.esprit.examen2019login.DescriptionActivity;
import tn.esprit.examen2019login.R;


public class ProduitAdapter extends RecyclerView.Adapter<ProduitAdapter.ViewHolder> {
    private Context mContext;
    private List<Produit> paysList;
    @Override
    public ProduitAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = LayoutInflater.from(mContext).inflate(R.layout.row_produit, parent, false);
        return new ViewHolder(mItemView);
    }
    @Override
    public void onBindViewHolder(final ProduitAdapter.ViewHolder holder, final int position) {
        final Produit singleItem = paysList.get(position);
        holder.txtPays.setText(singleItem.getLibelle());
        if (singleItem.getType().equals("Samsung"))
        holder.img.setImageResource(R.drawable.ic_samsung);
        if (singleItem.getType().equals("Apple"))
            holder.img.setImageResource(R.drawable.ic_apple);
        if (singleItem.getType().equals("Huawei"))
            holder.img.setImageResource(R.drawable.ic_huawei);
        holder.prix.setText(singleItem.getPrix()+"DT");



        holder.line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(mContext, DescriptionActivity.class);
                intent.putExtra("film", (Serializable) singleItem);

                mContext.startActivity(intent);
            }
        });



    }
    @Override
    public int getItemCount() {
        return paysList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtPays;
        private ImageView img ;
        private TextView prix;
        LinearLayout line;

        //A changer -> filmAdapter
        public ViewHolder(View itemView) {
            super(itemView);

            txtPays = itemView.findViewById(R.id.na);
            img = itemView.findViewById(R.id.imageView);
            prix = itemView.findViewById(R.id.prix);
            line = itemView.findViewById(R.id.ll);

        }
    }

    public ProduitAdapter(Context mContext, List<Produit> paysList) {
        this.mContext = mContext;
        this.paysList = paysList;
    }

}
