package tn.esprit.examen2019login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    Button signIn;
    EditText login;
    EditText password;
    private SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        preferences =this.getSharedPreferences("connected", Context.MODE_PRIVATE);


        boolean connected=preferences.getBoolean("con",false);
        if (connected){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }
        login=(EditText) findViewById(R.id.etlogin);
        password=(EditText) findViewById(R.id.etPassword);
        signIn=(Button) findViewById(R.id.bsignUp);
    }

    private void toastMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    public void onClickSingin(View view) {

            SharedPreferences.Editor editor= preferences.edit();
            editor.putBoolean("con",true);
            editor.putString("pseudo",login.getText().toString());
            editor.putString("motdePasse",password.getText().toString());
            editor.apply();
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra("nom",login.getText().toString());
            startActivity(intent);
            finish();

    }


}
