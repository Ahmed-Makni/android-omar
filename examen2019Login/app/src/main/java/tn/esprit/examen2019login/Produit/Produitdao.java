package tn.esprit.examen2019login.Produit;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Produitdao {

    @Query("Select * FRom Produit")
    List<Produit> getallpays();
    @Insert
    void inserall(Produit films);
    @Delete
    void delete(Produit film);
    @Update
    void updte(Produit film);
}
