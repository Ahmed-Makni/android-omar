package tn.esprit.examen2019login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import tn.esprit.examen2019login.Produit.AppDatabase;
import tn.esprit.examen2019login.Produit.Produit;


public class DescriptionActivity extends AppCompatActivity {


    private Button sup;
    private TextView dessc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        final Bundle B =getIntent().getExtras();
        final AppDatabase db = Room.databaseBuilder(getApplicationContext(),AppDatabase.class,"phhhh")
                .allowMainThreadQueries()
                .build();
        dessc = findViewById(R.id.de);
        sup = findViewById(R.id.supp);
        Produit film= (Produit) B.get("film");
        System.out.println("aaaaaaa"+film);
        dessc.setText(film.getLibelle());

        sup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Produit film= (Produit) B.get("film");
                System.out.println("aaaaaaa"+film);

                db.produitdao().delete(film);
                Intent intent = new Intent(DescriptionActivity.this, MainActivity.class);
                startActivity(intent);

            }
        });
    }
}
