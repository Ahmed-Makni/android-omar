package tn.esprit.examen2019login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import tn.esprit.examen2019login.Produit.AppDatabase;
import tn.esprit.examen2019login.Produit.Produit;

public class AjouterProduitActivity extends AppCompatActivity {

    Button btnAddPRoduit;
    EditText etLibel;
    EditText etPrix;
    EditText etQuantite;
    RadioGroup type;

    AppDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_produit);
        db= Room.databaseBuilder(getApplicationContext(), AppDatabase.class,"phhhh")
                .allowMainThreadQueries()
                .build();
        etLibel= (EditText) findViewById(R.id.etLibelle);
        etQuantite= (EditText) findViewById(R.id.etQuantite);
        etPrix= (EditText) findViewById(R.id.extPrix);
        type= (RadioGroup) findViewById(R.id.radioButton);
    }

    public void OnclickEnregistrerProduit(View view) {

        Produit p=new Produit();
        p.setLibelle(etLibel.getText().toString());
        p.setQuantite(etQuantite.getText().toString());
        p.setPrix(etPrix.getText().toString());
        p.setImage("ic_apple.png");
        p.setType("Apple");
        db.produitdao().inserall(p);
        Intent intent = new Intent(AjouterProduitActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
