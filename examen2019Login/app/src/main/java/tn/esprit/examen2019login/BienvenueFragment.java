package tn.esprit.examen2019login;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class BienvenueFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "nom";
    Button logout;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public BienvenueFragment() {
        // Required empty public constructor
    }


    public static BienvenueFragment newInstance(String param1) {
        BienvenueFragment fragment = new BienvenueFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_bienvenue, container, false);
        logout=view.findViewById(R.id.logout);
//        logout.setOnClickListener((event)->{
//            Intent intent = new Intent(getContext(), LoginActivity.class);
//            startActivity(intent);
//        });
        return view;
    }

}
