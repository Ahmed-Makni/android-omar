package tn.esprit.examen2019login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView nomUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Bundle B =getIntent().getExtras();
      //  nomUser.setText(B.getString("nom"));
        nomUser= (TextView) findViewById(R.id.textUser);
        nomUser.setText("ahmed");
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.bienvenueFragment);
        if(fragment == null){
            getSupportFragmentManager().beginTransaction().add(R.id.bienvenueFragment, BienvenueFragment.newInstance(nomUser.getText().toString())).commit();
        }

        Fragment fragment2 = getSupportFragmentManager().findFragmentById(R.id.listeProduitfragment);
        if(fragment2 == null){
            getSupportFragmentManager().beginTransaction().add(R.id.listeProduitfragment, ListProduitFragment.newInstance()).commit();
        }



    }

    public void onClickAdd(View view) {
        Intent intent = new Intent(MainActivity.this, AjouterProduitActivity.class);
        startActivity(intent);
    }
}
