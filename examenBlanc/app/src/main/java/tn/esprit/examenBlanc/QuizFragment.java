package tn.esprit.examenBlanc;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class QuizFragment extends Fragment {

    TextView nomText;
    TextView ageText;
    private String mName;
    private int mAge;


    public QuizFragment() {
        // Required empty public constructor
    }


    public static QuizFragment newInstance(String nom, int age) {
        QuizFragment fragment = new QuizFragment();
        Bundle args = new Bundle();
        args.putString("nom", nom);
        args.putInt("age", age);
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_quiz, container, false);
        nomText=view.findViewById(R.id.txtNom);
        ageText=view.findViewById(R.id.txtAge);

        mName = getArguments().getString("nom");
        mAge = getArguments().getInt("age");
        nomText.setText(mName);
        ageText.setText(mAge+"");
        return view;

    }


}
