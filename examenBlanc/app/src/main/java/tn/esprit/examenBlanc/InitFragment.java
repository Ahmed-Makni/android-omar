package tn.esprit.examenBlanc;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;


public class InitFragment extends Fragment {

    private EditText mNomInput;
    private TextView mDateInput;
    private Button pickDate;
    private Button mCommencerButton;
    Calendar calendar;
    DatePickerDialog datePickerDialog;
    private int year;
    private int age;

    public int getAge() {
        return age;
    }

    public static InitFragment newInstance() {

        Bundle args = new Bundle();

        InitFragment fragment = new InitFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_init, container, false);
        mNomInput = view.findViewById(R.id.nom_input);
        mDateInput = view.findViewById(R.id.txtDate);
        mCommencerButton = view.findViewById(R.id.commencer_button);
        pickDate = view.findViewById(R.id.btnPick);
        pickDate.setOnClickListener((event)->{
            calendar=Calendar.getInstance();
            int day=calendar.get(Calendar.DAY_OF_MONTH);
            int month=calendar.get(Calendar.MONTH);
           year=calendar.get(Calendar.YEAR);
            datePickerDialog=new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                    mDateInput.setText(mDay + "/"+ (mMonth + 1) + "/" + mYear);
                    year=mYear;
                }
            },year,month,day);
            datePickerDialog.show();
        });
        mCommencerButton.setOnClickListener((event)->{
            Date date=new Date();
           int yearNow= date.getYear()+1900;
            System.out.println(yearNow);
            System.out.println(year);
            age =yearNow-year;
            System.out.println(age);
            System.out.println("nom  " +mNomInput.getText().toString());
            System.out.println("date  " +mDateInput.getText().toString());
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.initfragment, QuizFragment.newInstance(mNomInput.getText().toString(), age)).commit();

        });
        return view;
    }

}
