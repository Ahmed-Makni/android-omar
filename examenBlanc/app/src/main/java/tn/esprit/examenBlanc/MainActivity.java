package tn.esprit.examenBlanc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.initfragment);
        if(fragment == null){
            getSupportFragmentManager().beginTransaction().add(R.id.initfragment, InitFragment.newInstance()).commit();
        }
    }
}
